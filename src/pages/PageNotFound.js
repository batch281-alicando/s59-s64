import { Link} from 'react-router-dom';

export default function NotFound (){


  return (
     <div>
      <h1>404 - Page Not Found</h1>
      <p>The requested page could not be found. Go to <Link to="/">homepage</Link>.</p>
    </div>
  );
}



